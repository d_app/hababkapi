<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            ["key" => "currency", "value" => "INR"],
            ["key" => "delivery_fee", "value" => "25"],
            ["key" => "admin_fee_for_order_in_percent", "value" => "10"],
            ["key" => "tax_in_percent", "value" => "10"],
            ["key" => "support_email", "value" => "admin@example.com"],
            ["key" => "support_phone", "value" => "8181818118"],
            ["key" => "send_welcome_email", "value" => "1"],
            ["key" => "send_order_placed_email", "value" => "1"],
            ["key" => "send_order_complete_email", "value" => "1"],
            ["key" => "cod_enabled", "value" => "1"],
        ]);
    }
}
