<?php

namespace App\Listeners;

use App\Events\Ordered;
use App\Events\PostOrder;
use App\Helpers\PushNotificationHelper;
use App\Mail\OrderPlaced;
use App\Models\Auth\User\User;
use App\Models\DeliveryProfile;
use App\Models\Earning;
use App\Models\Order;
use App\Models\OrderStatusLog;
use App\Models\PushNotification;
use App\Models\Setting;
use App\Notifications\Admin\NewOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderListener
{
    private $order;
    private $event;
    private $pushNotifications;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Ordered $event
     * @return void
     */
    public function handle(Ordered $event)
    {
        try {
            $this->event = $event;
            $this->order = $event->order;
            $this->pushNotifications = [];

            $this->_statusUpdate();

            $this->_deliveryStatusUpdate();

            event(new PostOrder($event->order, $this->pushNotifications));
        } catch (\Exception $ex) {
            Log::error('Exception occurred', [$ex->getMessage(), $ex->getTraceAsString()]);
        }
    }

    private function _statusUpdate()
    {
        if($this->event->statusUpdate) {
            OrderStatusLog::create(['order_id' => $this->order->id, 'status' => $this->order->status]);
        }

        if ($this->order->status == 'new' && $this->order->paymentMethod->slug == 'cod') {
            // send a notification to store
            $this->pushNotifications[] = new PushNotification($this->order->store->user->fcm_registration_id,
                'New Order', 'You have received new order', ["order_id" => $this->order->id]);
        }

        // when store accepts the order, send notification to user and try to allocate delivery boy for the order
        if ($this->order->status == 'accepted') {

            // try to allot a delivery person
            if ($this->order->delivery_status == 'pending') {
                $this->_allotDeliveryPerson();
            }

            // send notification to user that his order accepted by user
            $this->pushNotifications[] = new PushNotification($this->order->user->fcm_registration_id,
                'Order Accepted', 'Your order has been accepted by store', ["order_id" => $this->order->id]);
        }


        if ($this->order->status == 'preparing') {

            // try to allot a delivery person, if not yet done
            if ($this->order->delivery_status == 'pending') {
                $this->_allotDeliveryPerson();
            }

            $this->pushNotifications[] = new PushNotification($this->order->user->fcm_registration_id,
                'Order Preparing', 'Store has started preparing your order', ["order_id" => $this->order->id]);
        }

        if ($this->order->status == 'dispatched') {

            $this->pushNotifications[] = new PushNotification($this->order->user->fcm_registration_id,
                'Order Dispatched', 'Restaurant has dispatched your order', ["order_id" => $this->order->id]);
        }

        if ($this->order->status == 'rejected') {
            $this->pushNotifications[] = new PushNotification($this->order->user->fcm_registration_id,
                'Order Rejected', 'Sorry! Your order has been rejected by store', ["order_id" => $this->order->id]);
        }
    }

    private function _deliveryStatusUpdate()
    {
        if ($this->order->delivery_status == 'complete') {
            $this->_onComplete();
        }
    }

    private function _allotDeliveryPerson()
    {
        $deliveryProfiles = DeliveryProfile::search($this->order->store_id);
        // $deliveryProfiles = DeliveryProfile::where('assigned', 0)->get();
        if (count($deliveryProfiles) > 0) {
            $deliveryProfile = $deliveryProfiles[0];
            $this->order->delivery_profile_id = $deliveryProfile->id;
            $this->order->delivery_status = 'allotted';
            $this->order->save();

            // set the assigned field of delivery boy to true implying delivery boy is not available for pickup
            $deliveryProfile->assigned = true;
            $deliveryProfile->save();

            // send notification to delivery person
            $this->pushNotifications[] = new PushNotification($deliveryProfile->user->fcm_registration_id,
                'New Delivery', 'You have received new delivery order', ["order_id" => $this->order->id]);
        }
    }

    private function _onComplete()
    {
        // when delivery is complete, update the order's status to complete
        $this->order->status = 'complete';
        $this->order->save();
        OrderStatusLog::create(['order_id' => $this->order->id, 'status' => $this->order->status]);

        // when order is complete, set assigned field of delivery boy to false implying delivery boy is now available for pickup
        $deliveryProfile = $this->order->deliveryProfile;
        $deliveryProfile->assigned = false;
        $deliveryProfile->save();

        // transfer earnings to store and delivery person
        # store earnings
        $adminShareInOrder = floatval(Setting::where('key', 'admin_fee_for_order_in_percent')->first()->value);
        $storeEarnings = new Earning();
        $storeEarnings->order_id = $this->order->id;
        $storeEarnings->amount = $this->order->subtotal - ($this->order->subtotal * $adminShareInOrder) / 100;
        $storeEarnings->user_id = $this->order->store->user->id;
        $storeEarnings->save();

        $this->pushNotifications[] = new PushNotification($this->order->store->user->fcm_registration_id,
            'Order Complete', 'Order is complete. Earnings credited', ["order_id" => $this->order->id]);

        # delivery person earnings
        $deliveryFee = floatval(Setting::where('key', 'delivery_fee')->first()->value);
        $deliveryEarnings = new Earning();
        $deliveryEarnings->order_id = $this->order->id;
        $deliveryEarnings->amount = $deliveryFee;
        $deliveryEarnings->user_id = $this->order->deliveryProfile->user->id;
        $deliveryEarnings->save();

        $this->pushNotifications[] = new PushNotification($this->order->deliveryProfile->user->fcm_registration_id,
            'Order Complete', 'Order is complete. Earnings credited', ["order_id" => $this->order->id]);
    }
}
